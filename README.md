# Auction.io

## Intsallation

 1. rake db:setup
 2. rake db:migrate
 3. rake db:setup

## Requirements

Main requirement is having Redis installed, otherwise Sidekiq will not work.
I decided to use Sidekiq to process all the payments through it, so if something goes wrong - we have a history of payments in Redis

## Instructions

There is not much to tell, I hope I made everything pretty clear in terms of experience :) You can change your balance with the top up feature in the user dropdown, if needed.

There is a rake task to activate offers that simulates a production behavior with some kind of scheduler. You should run it every time you create new products, so you can bid on them.

* rake force_activate_offers
