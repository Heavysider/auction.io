# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Generate Users
user_ids = []
(1..10).each do |index|
  random_email = Faker::Internet.unique.email
  user = User.new(email: random_email, password: random_email, password_confirmation: random_email, balance: 500)
  user.save!(validate: false)
  user_ids << user.id
end

(1..100).each do |index|
  lot = Lot.new(
    user_id: user_ids.sample,
    description: Faker::Lorem.paragraph_by_chars(512),
    start_date: (DateTime.now - 2.days).beginning_of_day,
    end_date: (DateTime.now + rand(3..6).days).end_of_day,
    name: Faker::Superhero.name,
    price: rand(50..400)
  )
  lot.save!(validate: false)
end

Lot.where(status: 'inactive').update_all(status: 'active')
