class ChangeLotsTable < ActiveRecord::Migration[5.2]
  def change
    add_column :lots, :start_date, :datetime
    add_column :lots, :end_date, :datetime
    add_column :lots, :current_highest_bid, :float
    add_column :lots, :current_highest_bidder, :integer
  end
end
