class AddPriceToLots < ActiveRecord::Migration[5.2]
  def change
    add_column :lots, :price, :float
  end
end
