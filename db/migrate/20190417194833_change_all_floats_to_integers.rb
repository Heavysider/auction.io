class ChangeAllFloatsToIntegers < ActiveRecord::Migration[5.2]
  def change
    change_column :bids, :bid_amount, :integer
    change_column :lots, :current_highest_bid, :integer
    change_column :payments, :amount, :integer
    change_column :users, :balance, :integer
  end
end
