class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.integer :user_id_from
      t.integer :user_id_to
      t.float :amount
      t.integer :bid_id

      t.timestamps
    end
  end
end
