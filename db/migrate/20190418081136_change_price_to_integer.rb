class ChangePriceToInteger < ActiveRecord::Migration[5.2]
  def change
    change_column :lots, :price, :integer
  end
end
