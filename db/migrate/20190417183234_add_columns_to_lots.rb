class AddColumnsToLots < ActiveRecord::Migration[5.2]
  def change
    add_column :lots, :name, :string
    add_column :lots, :status, :string
  end
end
