class RenameProductIdToLotId < ActiveRecord::Migration[5.2]
  def change
    rename_column :bids, :product_id, :lot_id
  end
end
