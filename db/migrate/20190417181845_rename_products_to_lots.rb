class RenameProductsToLots < ActiveRecord::Migration[5.2]
  def change
    rename_table :products, :lots
  end
end
