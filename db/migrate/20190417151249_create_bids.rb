class CreateBids < ActiveRecord::Migration[5.2]
  def change
    create_table :bids do |t|
      t.integer :user_id
      t.integer :product_id
      t.float :bid_amount

      t.timestamps
    end
  end
end
