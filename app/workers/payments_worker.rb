class PaymentsWorker
  include Sidekiq::Worker

  def perform(from_user_id, to_user_id, amount, bid_id, lot_id)
    owner = User.find(to_user_id)
    owner.balance += amount
    owner.save!
    Payment.create!(user_id_from: from_user_id, user_id_to: to_user_id, amount: amount, bid_id: bid_id, lot_id: lot_id)
  end
end
