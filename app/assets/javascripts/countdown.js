// Set the date we're counting down to
window.addEventListener('load', countdownStart);

function countdownStart () {
  let element = document.getElementById('countdown-timer')
  if (element) {
    if (element.dataset.status === 'active') {
      const countDownDate = new Date(element.dataset.endDate).getTime();
      // Update the count down every 1 second
      let x = setInterval(function () {

        // Get todays date and time
        const now = new Date().getTime();

        // Find the distance between now and the count down date
        const distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        const days = Math.floor(distance / (1000 * 60 * 60 * 24));
        const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="countdown-timer"
        document.getElementById("countdown-timer").innerHTML = `${days}d ${hours}h ${minutes}m ${seconds}s`;

        // If the count down is finished, write some text
        if (distance < 0) {
          clearInterval(x);
          document.getElementById("countdown-timer").innerHTML = "EXPIRED";
        }
      }, 1000);
    } else {
      document.getElementById("countdown-timer").innerHTML = "CLOSED"
    }
  }
};
