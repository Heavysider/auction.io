

window.addEventListener('load', initializeBidding)

function initializeBidding() {
  bidButton = document.getElementById('button-addon2 make-a-bid')
  if (bidButton) {
    document.getElementById('button-addon2 make-a-bid').addEventListener('click', bid)
    let biddingInProgress = false

    function bid() {
      if (!biddingInProgress) {
        biddingInProgress = true
        dataObject = document.getElementById('bid-amount')
        data = {
          lot_id: dataObject.dataset.lotId,
          bid_amount: dataObject.value
        }

        fetch('/bid', {
          method: 'POST',
          credentials: "include",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then((data) => {
          alert(data.message);
          biddingInProgress = false;
        })
      } else {
        alert('Your previous bid is still in progress');
      }
    }
  }
}

// fetch(url, {
//   method: "POST", // *GET, POST, PUT, DELETE, etc.
//   mode: "cors", // no-cors, cors, *same-origin
//   cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
//   credentials: "same-origin", // include, *same-origin, omit
//   headers: {
//     "Content-Type": "application/json",
//   },
//   redirect: "follow", // manual, *follow, error
//   referrer: "no-referrer", // no-referrer, *client
//   body: JSON.stringify(data), // body data type must match "Content-Type" header
// })
// .then(response => response.json())
// .then(data => console.log(JSON.stringify(data)))
