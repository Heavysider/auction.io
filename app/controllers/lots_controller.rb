class LotsController < ApplicationController
  def index
    @lots = Lot.order('created_at desc').page params[:page]
  end

  def new
    @lot = Lot.new
  end

  def create
    @lot = Lot.new(lot_params)
    @lot.user_id = current_user.id
    if @lot.save
      redirect_to root_path
    else
      render :new
    end
  end

  def show
    @lot = Lot.find(params[:id])
  end

  private

  def lot_params
    params.require(:lot).permit(:name, :description, :start_date, :end_date, :price)
  end

end
