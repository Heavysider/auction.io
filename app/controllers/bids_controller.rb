class BidsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def bid
    ActiveRecord::Base.transaction do
      lot = Lot.lock.find(params[:lot_id])
      result, message = current_user.can_bid?(bid_amount: params[:bid_amount].to_i, lot: lot)
      if result
        bid = Bid.create!(user_id: current_user.id, lot_id: lot.id, bid_amount: params[:bid_amount].to_i)
        lot.return_money_previous_bidder(current_user)
        current_user.take_money(params[:bid_amount].to_i)
        lot.finalize_bid(amount: params[:bid_amount].to_i, user_id: current_user.id, bid_id: bid.id)
        render json: { status: 200, message: 'Bidding was successful' }, status: 200
      else
        render json: { status: 401, message: message }, status: 401
      end
    end
  end
end
