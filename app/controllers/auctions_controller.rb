class AuctionsController < ApplicationController
  def index
    @lots = Lot.active.order('created_at desc').page params[:page]
  end

end
