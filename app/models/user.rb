class User < ApplicationRecord
  has_secure_password

  validates :email, presence: true, uniqueness: true
  has_many :payments, foreign_key: :user_id_from

  before_create :set_defaults

  def can_bid?(bid_amount:, lot:)
    return [false, 'This lot has been sold already'] if lot.status != 'active'
    return [false, "You can't bid on your lots"] if lot.user_id == self.id
    return [false, 'Your bid is lower than current highest bid'] if lot.current_highest_bid >= bid_amount
    return [false, 'Your bid is higher than your current balance'] if bid_amount > self.balance
    [true, nil]
  end

  def add_money(amount)
    self.balance += amount
    self.save!
  end

  def take_money(amount)
    self.balance -= amount
    self.save!
  end

  def bought_lots
    Lot.where(id: self.payments.pluck(:lot_id))
  end

  private

  def set_defaults
    self.balance = 0 if self.balance.nil?
  end
end
