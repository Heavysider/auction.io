class Lot < ApplicationRecord
  paginates_per 12

  has_many :bids

  validates :name, presence: true, uniqueness: true
  validates :start_date, presence: true
  validates :end_date, presence: true
  validate :start_date_shoud_start_tomorrow, on: :create
  validate :end_date_shoud_be_in_future, on: :create
  validate :end_date_earlier_than_start_date, on: :create

  before_create :set_defaults

  scope :active, -> { where(status: 'active') }

  def active?
    self.status == 'active'
  end

  def finalize_bid(amount:, user_id:, bid_id:)
    self.current_highest_bid = amount
    self.current_highest_bidder = user_id
    self.status = 'closed' if amount >= self.price
    self.save!
    PaymentsWorker.perform_async(user_id, self.user_id, amount, bid_id, self.id) if amount >= self.price
  end

  def return_money_previous_bidder(current_user)
    if self.current_highest_bidder
      if self.current_highest_bidder == current_user.id
        current_user.add_money(self.current_highest_bid)
      else
        previous_bidder = User.find(self.current_highest_bidder)
        previous_bidder.add_money(self.current_highest_bid)
      end
    end
  end

  private

  def set_defaults
    self.current_highest_bid = 0
    self.end_date = self.end_date.end_of_day
    self.status = 'inactive'
  end

  def start_date_shoud_start_tomorrow
    if self.start_date.present? && self.start_date < DateTime.tomorrow.beginning_of_day
      errors.add(:start_date, "should start from tomorrow or further in future")
    end
  end

  def end_date_shoud_be_in_future
    if self.end_date.present? && self.end_date < (DateTime.now + 2.days).beginning_of_day
      errors.add(:start_date, "should start from tomorrow or further in future")
    end
  end

  def end_date_earlier_than_start_date
    if self.start_date.present? && self.end_date.present? && self.end_date < self.start_date
      errors.add(:end_date, "can't be earlier than Start Date")
    end
  end
end


