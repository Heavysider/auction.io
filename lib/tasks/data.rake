task :force_activate_offers => :environment do
  Lot.where(status: 'inactive').update_all(status: 'active')
end
